'use strict';

// Pictures controller
angular.module('pictures').controller('PicturesController', ['$scope', '$stateParams', '$location', '$upload', 'Authentication', 'Pictures',
	function($scope, $stateParams, $location, $upload, Authentication, Pictures ) {
		$scope.authentication = Authentication;
		// image upload handle
		$scope.onFileSelect = function(image) {
			if(angular.isArray(image)) {
				image = image[0];
			}
			if (image.type !== 'image/png' && image.type !== 'image/jpeg') {
        alert('Only PNG and JPEG are accepted.');
        return;
      }
      $scope.uploadInProgress = true;
      $scope.uploadProgress = 0;
      $scope.upload = $upload.upload({
      	url: '/pictures/upload',
      	method: 'POST',
      	file: image
      }).progress(function(event) {
      	$scope.uploadProgress = Math.floor(event.loaded / event.total);
  			$scope.$apply();
      }).success(function(data, status, headers, config) {
      	$scope.uploadInProgress = false;
      	$scope.uploadedImage = angular.fromJson(data);
      }).error(function(err) {
      	$scope.uploadInProgress = false;
	    	$scope.error = err.message || err;
      });
		};
		// Create new Picture
		$scope.create = function() {
			// Create new Picture object
			var picture = new Pictures ({
				name: this.name,
				alt: this.alt,
				title: this.title,
				public_url: this.uploadedImage.public_url,
				private_url: this.uploadedImage.private_url
			});

			// Redirect after save
			picture.$save(function(response) {
				$location.path('pictures/' + response._id);

				// Clear form fields
				$scope.name = '';
        $scope.alt = '';
        $scope.title = '';
        $scope.uploadedImage = '';  
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Picture
		$scope.remove = function( picture ) {
			if ( picture ) { picture.$remove();

				for (var i in $scope.pictures ) {
					if ($scope.pictures [i] === picture ) {
						$scope.pictures.splice(i, 1);
					}
				}
			} else {
				$scope.picture.$remove(function() {
					$location.path('pictures');
				});
			}
		};

		// Update existing Picture
		$scope.update = function() {
			var picture = $scope.picture ;

			picture.$update(function() {
				$location.path('pictures/' + picture._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Pictures
		$scope.find = function() {
			$scope.pictures = Pictures.query();
		};

		// Find existing Picture
		$scope.findOne = function() {
			$scope.picture = Pictures.get({ 
				pictureId: $stateParams.pictureId
			});
		};
	}
]);
