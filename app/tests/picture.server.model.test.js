'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Picture = mongoose.model('Picture');

/**
 * Globals
 */
var user, picture, picture2;

/**
 * Unit tests
 */
describe('Picture Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});
		user.save(function() { 
			picture = new Picture({
				name: 'Picture Name',
				alt: 'Alt description',
				title: 'Picture title',
				url: 'public/modules/pictures/img/image.jpg',
				user: user
			});
			picture2 = new Picture({
				name: 'Picture Name',
				alt: 'Alt description',
				title: 'Picture title',
				url: 'public/modules/pictures/img/image.jpg',
				user: user
			});
		});
		done();
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return picture.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should fail to save the same image twice', function(done) {
			picture.save();
			return picture2.save(function(err) {
				should.exist(err);
				done();
			});
		});

		it('should be able to show an error when try to save without name', function(done) { 
			picture.name = '';
			return picture.save(function(err) {
				should.exist(err);
				done();
			});
		});

		it('should be able to show an error when try to save without public url', function(done) {
			picture.public_url = '';
			return picture.save(function(err) {
				should.exist(err);
				done();
			});
		});
		it('should be able to show an error when try to save without private url', function(done) {
			picture.private_url = '';
			return picture.save(function(err) {
				should.exist(err);
				done();
			});
		});
		it('should be able to save without alt nor title filled', function(done) {
			picture.alt = '';
			picture.title = '';
			return picture.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

	});
  
	afterEach(function(done) { 
		Picture.remove().exec();
		User.remove().exec();

		done();
	});
});
