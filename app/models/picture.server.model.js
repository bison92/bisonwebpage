'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Picture Schema
 */
var PictureSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Por favor de un nombre a la imagen',
		trim: true
	},
	alt: {
		type: String,
		default: '',
		trim: true
	},
	title: {
		type: String,
		default: '',
		trim: true
	},
	public_url: {
		type: String,
		unique: 'No pueden existir dos imágenes con la misma ruta',
		default: 'public/modules/pictures/img/',
		required: 'Suba una imagen',
		trim: true
	},
	private_url: {
		type: String,
		unique: 'No pueden existir dos imágenes con la misma ruta',
		default: './modules/public/pictures/img/',
		required: 'Suba una imagen',
		trim: true
	},
	created_at: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Picture', PictureSchema);
