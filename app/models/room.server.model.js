'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Room Schema
 */

var RoomSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Por favor rellene el nombre',
		trim: true
	},
	photos: [{type: Schema.ObjectId, ref: 'Picture'}],
	created_at: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Room', RoomSchema);
